package ghc2021.parser;

import ghc2021.executor.TrafficSignalSolver;
import ghc2021.model.Car;
import ghc2021.model.Intersection;
import ghc2021.model.Street;

import java.util.*;

public class TrafficSignalParser {

    public TrafficSignalSolver parse(Scanner sc) {
        String firstLine = sc.nextLine();
        String[] s = firstLine.split(" ");
        int duration = Integer.parseInt(s[0]);
        int numberOfIntersection = Integer.parseInt(s[1]);
        int numberOfStreet = Integer.parseInt(s[2]);
        int numberOfCars = Integer.parseInt(s[3]);
        int bonus = Integer.parseInt(s[4]);
        Map<String, Street> streets = new HashMap<>();
        Map<Integer, Intersection> intersections = new HashMap<>();
        for (int i = 0; i < numberOfStreet; i++) {
            String[] streeDescriptor = sc.nextLine().split(" ");
            String streetName = streeDescriptor[2];
            int startIntersection = Integer.parseInt(streeDescriptor[0]);
            int endIntersection = Integer.parseInt(streeDescriptor[1]);
            intersections.computeIfAbsent(startIntersection, Intersection::new).outputStreet(streetName);
            intersections.computeIfAbsent(endIntersection, Intersection::new).inputStreet(streetName);
            streets.put(streetName, new Street(streetName, startIntersection, endIntersection, Integer.parseInt(streeDescriptor[3])));
        }
        List<Car> carList = new ArrayList<>();
        for (int i = 0; i < numberOfCars; i++) {
            String[] carDescriptor = sc.nextLine().split(" ");
            carList.add(new Car(i, Integer.parseInt(carDescriptor[0]), Arrays.asList(carDescriptor).subList(1, carDescriptor.length)));
        }
        return new TrafficSignalSolver(duration, numberOfIntersection, bonus, streets, intersections, carList);
    }
}
