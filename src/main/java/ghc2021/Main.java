package ghc2021;

import ghc2021.executor.TrafficSignalSolver;
import ghc2021.parser.TrafficSignalParser;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {

    private static final String baseInput = "src/main/resources/ghc2021/input/";
    private static final String baseOutput = "src/main/resources/ghc2021/output/";
    private static final String OUT = ".out";
    private static final String IN = ".txt";

    private static final String[] inputNames = {"a", "b", "c", "d", "e", "f"};
    private final static TrafficSignalParser parser = new TrafficSignalParser();

    public static void main(String[] args) throws FileNotFoundException {
        for (String input : inputNames) {
            try (Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(baseInput + input + IN)))) {
                System.out.println("Start input: " + input);
                TrafficSignalSolver trafficSignal = parser.parse(sc);
                trafficSignal.schedule(baseOutput + input + OUT);
                System.out.println("End input: " + input);
            }
        }
    }
}
