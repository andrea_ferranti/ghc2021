package ghc2021.model;

import java.util.Objects;

public class Street {
    private String name;
    private final int start;
    private final int end;
    private final int duration;

    public Street(String name, int start, int end, int duration) {
        this.name = name;
        this.start = start;
        this.end = end;
        this.duration = duration;
    }

    public String getName() {
        return name;
    }

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }

    public int getDuration() {
        return duration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Street street = (Street) o;
        return Objects.equals(name, street.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
