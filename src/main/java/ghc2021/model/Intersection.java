package ghc2021.model;

import java.util.*;
import java.util.stream.Collectors;

public class Intersection {
    private int id;
    private Set<String> outputStreet;
    private Map<String, Integer> streets;
    private LinkedHashSet<String> sortedStreet;

    public Intersection(int id) {
        this.id = id;
        this.streets = new HashMap<>();
        this.sortedStreet = new LinkedHashSet<>();
        this.outputStreet = new HashSet<>();
    }

    public void outputStreet(String streetName) {
        this.outputStreet.add(streetName);
    }

    public void inputStreet(String streetName) {
        this.streets.put(streetName, -1);
        this.sortedStreet.add(streetName);
    }

    public boolean isScheduleDefined(){
        return streets.entrySet().stream().anyMatch(e -> e.getValue() > 0);
    }

    public LinkedHashSet<String> getInputStreet(){
        return sortedStreet;
    }

    public void addSchedule(String street, int schedule){
        this.streets.put(street, schedule);
    }

    @Override
    public String toString() {
        String str = id + "\n";
        List<String> streetSchedules = streets.entrySet().stream().filter(e -> e.getValue() > 0).map(e -> String.format("%s %d", e.getKey(), e.getValue())).collect(Collectors.toList());
        String streetSchedule = streetSchedules.size() + "\n";
        return str + streetSchedule + String.join("\n", streetSchedules);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Intersection that = (Intersection) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public boolean isOutputOn(String outputStreet) {
        return this.outputStreet.contains(outputStreet);
    }
}
