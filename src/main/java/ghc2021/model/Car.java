package ghc2021.model;

import java.util.List;
import java.util.Objects;

public class Car {
    private int id;
    private final int numberOfStreet;
    private final List<String> streets;

    public Car(int id, int numberOfStreet, List<String> streets) {
        this.id = id;
        this.numberOfStreet = numberOfStreet;
        this.streets = streets;
    }

    public int getId() {
        return id;
    }

    public int getNumberOfStreet() {
        return numberOfStreet;
    }

    public List<String> getStreets() {
        return streets;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return id == car.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Car{" +
                "numberOfStreet=" + numberOfStreet +
                ", streets=" + streets +
                '}';
    }
}
