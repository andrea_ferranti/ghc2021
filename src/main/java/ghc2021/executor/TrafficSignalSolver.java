package ghc2021.executor;

import ghc2021.model.Car;
import ghc2021.model.Intersection;
import ghc2021.model.Street;
import practice.model.DeliveryResult;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.*;
import java.util.stream.Collectors;

public class TrafficSignalSolver {
    private final int duration;
    private final int numberOfIntersection;
    private final int bonus;
    private final Map<String, Street> streets;
    private final Map<Integer, Intersection> intersections;
    private final List<Car> carList;
    private Random random = new Random();

    public TrafficSignalSolver(int duration, int numberOfIntersection, int bonus, Map<String, Street> streets, Map<Integer, Intersection> intersections, List<Car> carList) {
        this.duration = duration;
        this.numberOfIntersection = numberOfIntersection;
        this.bonus = bonus;
        this.streets = streets;
        this.intersections = intersections;
        this.carList = carList;
    }

    public void schedule(String outPath) throws FileNotFoundException {
        for (Map.Entry<Integer, Intersection> integerIntersectionEntry : intersections.entrySet()) {
            Intersection intersection = integerIntersectionEntry.getValue();
            for (String street : intersection.getInputStreet()) {
                intersection.addSchedule(street, Math.min(getNumberOfIntersectionByOutputStreet(street), duration / 2));
            }
        }
        saveResult(outPath);
    }

    private int getNumberOfIntersectionByOutputStreet(String outputStreet) {
        return Math.max(1, this.intersections.values().stream().filter(e -> e.isOutputOn(outputStreet)).mapToInt(e -> e.getInputStreet().size()).sum());
    }


    private void saveResult(String outPath) throws FileNotFoundException {
        List<Intersection> intersections = this.intersections.values().stream().filter(Intersection::isScheduleDefined).collect(Collectors.toList());
        try (PrintWriter pw = new PrintWriter(new BufferedOutputStream(new FileOutputStream(outPath)))) {
            pw.println(intersections.size());
            pw.println(intersections.stream().map(Intersection::toString).collect(Collectors.joining("\n")));
        }
    }

}