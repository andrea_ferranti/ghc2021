package practice;

import practice.executor.TeamWorkPizza;
import practice.executor.TeamWorkPizzaFactory;
import practice.model.DeliveryResult;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {

    private static final String baseInput = "src/main/resources/input/";
    private static final String baseOutput = "src/main/resources/output/";
    private static final String OUT = ".out";
    private static final String IN = ".in";

    private static final String[] inputNames = {"b_little_bit_of_everything"};//{"a_example"};//,"b_little_bit_of_everything", "c_many_ingredients", "d_many_pizzas", "e_many_teams"};
    private final static TeamWorkPizzaFactory factory = new TeamWorkPizzaFactory();

    public static void main(String[] args) throws FileNotFoundException {
        for (String input : inputNames) {
            try (Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(baseInput + input + IN)))) {
                System.out.println("Start input: " + input);
                TeamWorkPizza teamWorkPizza = factory.parseInput(sc);
                DeliveryResult deliveryResult = teamWorkPizza.allocatePizza();
                deliveryResult.saveDelivery(baseOutput + input + OUT);
                System.out.println("End input: " + input);
            }
        }
    }
}
