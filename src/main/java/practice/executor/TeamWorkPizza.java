package practice.executor;

import practice.model.DeliveryResult;

public interface TeamWorkPizza {

    DeliveryResult allocatePizza();
}
