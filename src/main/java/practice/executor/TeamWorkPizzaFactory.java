package practice.executor;

import practice.executor.impl.DefaultTeamWorkPizza;
import practice.model.Pizza;
import practice.model.TeamsDescriptor;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class TeamWorkPizzaFactory {

    public TeamWorkPizza parseInput(Scanner sc) {
        int numberOfPizzas = sc.nextInt();
        int twoMemberTeam = sc.nextInt();
        int threeMemberTeam = sc.nextInt();
        int fourMemberTeam = sc.nextInt();
        Pizza[] pizzas = new Pizza[numberOfPizzas];
        for (int i = 0; i < numberOfPizzas; i++) {
            int numberOfIngredients = sc.nextInt();
            Set<String> ingredients = new HashSet<>();
            for (int j = 0; j < numberOfIngredients; j++) {
                ingredients.add(sc.next());
            }
            pizzas[i] = new Pizza(i, ingredients);
        }
        return new DefaultTeamWorkPizza(pizzas, new TeamsDescriptor(twoMemberTeam, threeMemberTeam, fourMemberTeam));
    }
}
