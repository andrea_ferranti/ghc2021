package practice.executor.impl;

import practice.executor.TeamWorkPizza;
import practice.model.DeliveryResult;
import practice.model.Pizza;
import practice.model.Team;
import practice.model.TeamsDescriptor;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class DefaultTeamWorkPizza implements TeamWorkPizza {

    private final Pizza[] pizzas;
    private final TeamsDescriptor teams;
    private final Map<Integer, Pizza> indexedPizza;

    public DefaultTeamWorkPizza(Pizza[] pizzas, TeamsDescriptor teamsDescriptor) {
        this.pizzas = pizzas;
        Map<Integer, Pizza> indexedPizza = Arrays.stream(pizzas).collect(Collectors.toMap(Pizza::getId, Function.identity()));
        this.indexedPizza = indexedPizza;
        this.teams = teamsDescriptor;
    }

    @Override
    public DeliveryResult allocatePizza() {
        DeliveryResult deliveryResult = new DeliveryResult();
        List<Pizza> sortedPizza = new ArrayList<>(Arrays.asList(pizzas));
        sortedPizza.sort(new SortByNumberOfIngredients());
        fillTeams(deliveryResult, sortedPizza, teams.getFourMember(), 4);
        fillTeams(deliveryResult, sortedPizza, teams.getThreeMember(), 3);
        fillTeams(deliveryResult, sortedPizza, teams.getTwoMember(), 2);
        return deliveryResult;
    }

    private void fillTeams(DeliveryResult deliveryResult, List<Pizza> sortedPizza, int numberOfTeams, int teamSize) {
        for (int t = 0; t < numberOfTeams; t++) {
            Optional<Team> teamAllocation = getTeamAllocation(sortedPizza, teamSize);
            if (teamAllocation.isPresent()) {
                Team team = teamAllocation.get();
                deliveryResult.addDelivery(team.getTeamMember(), team.getPizzaIds());
            } else {
                break;
            }
        }
    }

    private Optional<Team> getTeamAllocation(List<Pizza> sortedPizza, int teamSize) {
        if (sortedPizza.size() < teamSize) {
            return Optional.empty();
        }
        Team team = new Team(teamSize);
        team.addPizza(sortedPizza.remove(0));
        for (int t = 1; t < team.getTeamMember(); t++) {
            int maxScore = -1;
            int bestIndex = 0;
            for (int p = 0; p < sortedPizza.size(); p++) {
                int differentIngredients = (int) team.getNumberOfIngredients(indexedPizza, sortedPizza.get(p));
                if (maxScore <= differentIngredients) {
                    maxScore = differentIngredients;
                    bestIndex = p;
                }
            }
            team.addPizza(sortedPizza.remove(bestIndex));
        }
        return Optional.of(team);
    }

    private static class SortByNumberOfIngredients implements Comparator<Pizza> {

        @Override
        public int compare(Pizza o1, Pizza o2) {
            return Integer.compare(o2.getIngredients().size(), o1.getIngredients().size());
        }
    }
}
