package practice.model;

import java.util.*;
import java.util.stream.Collectors;

public class Team {
    private final int teamMember;
    private final List<Integer> pizzas;

    public Team(int teamMember) {
        this.teamMember = teamMember;
        this.pizzas = new ArrayList<>();
    }

    public int getTeamMember() {
        return teamMember;
    }

    public boolean addPizza(Pizza pizza) {
        if (this.pizzas.size() == teamMember) {
            return false;
        }
        this.pizzas.add(pizza.getId());
        return true;
    }

    public List<Integer> getPizzaIds() {
        return new ArrayList<>(pizzas);
    }


    public long getNumberOfIngredients(Map<Integer, Pizza> indexedPizza, Pizza newPizza) {
        Set<String> collect = this.pizzas.stream().flatMap(id -> indexedPizza.get(id).getIngredients().stream()).collect(Collectors.toSet());
        collect.addAll(newPizza.getIngredients());
        return collect.size();
    }

}
