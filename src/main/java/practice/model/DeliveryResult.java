package practice.model;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DeliveryResult {

    private final List<DeliveryDescriptor> deliveries;

    public DeliveryResult() {
        this.deliveries = new ArrayList<>();
    }

    public void addDelivery(int numberOfPerson, List<Integer> pizzas) {
        this.deliveries.add(new DeliveryDescriptor(numberOfPerson, pizzas));
    }

    public void saveDelivery(String path) {
        try (PrintWriter fos = new PrintWriter(new BufferedOutputStream(new FileOutputStream(path)))) {
            fos.println(deliveries.size());
            deliveries.forEach(fos::println);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static class DeliveryDescriptor {
        private final int numberOfPerson;
        private final List<Integer> pizzas;

        private DeliveryDescriptor(int numberOfPerson, List<Integer> pizzas) {
            this.numberOfPerson = numberOfPerson;
            this.pizzas = pizzas;
        }

        @Override
        public String toString() {
            return numberOfPerson + " " + pizzas.stream().sorted().
                    map(String::valueOf).collect(Collectors.joining(" "));
        }
    }
}
