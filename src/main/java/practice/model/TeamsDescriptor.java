package practice.model;

public class TeamsDescriptor {

    private final int twoMember;
    private final int threeMember;
    private final int fourMember;

    public TeamsDescriptor(int twoMember, int threeMember, int fourMember) {
        this.twoMember = twoMember;
        this.threeMember = threeMember;
        this.fourMember = fourMember;
    }

    public int getTwoMember() {
        return twoMember;
    }

    public int getThreeMember() {
        return threeMember;
    }

    public int getFourMember() {
        return fourMember;
    }
}
